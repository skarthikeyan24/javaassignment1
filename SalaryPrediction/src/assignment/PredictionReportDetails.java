package assignment;

public class PredictionReportDetails {
	private int currentPredictionyear;
	private double sartingSalary;
	private double incrementAmount;
	private double deductionsAmount;
	private double salaryGrowth;
	
	
	public int getCurrentPredictionyear() {
		return currentPredictionyear;
	}
	public void setCurrentPredictionyear(int currentPredictionyear) {
		this.currentPredictionyear = currentPredictionyear;
	}
	public double getSartingSalary() {
		return sartingSalary;
	}
	public void setSartingSalary(double sartingSalary) {
		this.sartingSalary = sartingSalary;
	}
	public double getIncrementAmount() {
		return incrementAmount;
	}
	public void setIncrementAmount(double incrementAmount) {
		this.incrementAmount = incrementAmount;
	}
	public double getDeductionsAmount() {
		return deductionsAmount;
	}
	public void setDeductionsAmount(double deductionsAmount) {
		this.deductionsAmount = deductionsAmount;
	}
	public double getSalaryGrowth() {
		return salaryGrowth;
	}
	public void setSalaryGrowth(double salaryGrowth) {
		this.salaryGrowth = salaryGrowth;
	}
}
