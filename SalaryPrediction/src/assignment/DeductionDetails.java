package assignment;

public class DeductionDetails {

	private double sartingSalary;
	private int deductionPercent;
	private int noOfdeductionsInYear;
	private double deductionsAmount;
	private int currentPredictionyear;
	
	
	public double getSartingSalary() {
		return sartingSalary;
	}
	public void setSartingSalary(double sartingSalary) {
		this.sartingSalary = sartingSalary;
	}
	public int getDeductionPercent() {
		return deductionPercent;
	}
	public void setDeductionPercent(int deductionPercent) {
		this.deductionPercent = deductionPercent;
	}
	public int getNoOfdeductionsInYear() {
		return noOfdeductionsInYear;
	}
	public void setNoOfdeductionsInYear(int noOfdeductionsInYear) {
		this.noOfdeductionsInYear = noOfdeductionsInYear;
	}
	public double getDeductionsAmount() {
		return deductionsAmount;
	}
	public void setDeductionsAmount(double deductionsAmount) {
		this.deductionsAmount = deductionsAmount;
	}
	public int getCurrentPredictionyear() {
		return currentPredictionyear;
	}
	public void setCurrentPredictionyear(int currentPredictionyear) {
		this.currentPredictionyear = currentPredictionyear;
	}
	
}
