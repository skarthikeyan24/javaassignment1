package assignment;

import java.io.Serializable;

public class SalaryDetails implements Serializable{
	
	private double sartingSalary;
	private int incrementPercent;
	private int incrementType;
	private double deductions;
	private int deductionsType;
	private int predictionYears;
	
	private double incrementAmount;
	private int noOfIncrementsInYear;
	private int noOfDeductionsInYear;
	private int currentPredictionyear;
	
	
	/**
	 * @return the sartingSalary
	 */
	public double getSartingSalary() {
		return sartingSalary;
	}
	/**
	 * @param sartingSalary the sartingSalary to set
	 */
	public void setSartingSalary(double sartingSalary) {
		this.sartingSalary = sartingSalary;
	}
	/**
	 * @return the incrementPercent
	 */
	public int getIncrementPercent() {
		return incrementPercent;
	}
	/**
	 * @param incrementPercent the incrementPercent to set
	 */
	public void setIncrementPercent(int incrementPercent) {
		this.incrementPercent = incrementPercent;
	}
	/**
	 * @return the incrementType
	 */
	public int getIncrementType() {
		return incrementType;
	}
	/**
	 * @param incrementType the incrementType to set
	 */
	public void setIncrementType(int incrementType) {
		this.incrementType = incrementType;
	}
	/**
	 * @return the deductions
	 */
	public double getDeductions() {
		return deductions;
	}
	/**
	 * @param deductions the deductions to set
	 */
	public void setDeductions(double deductions) {
		this.deductions = deductions;
	}
	/**
	 * @return the deductionsType
	 */
	public int getDeductionsType() {
		return deductionsType;
	}
	/**
	 * @param deductionsType the deductionsType to set
	 */
	public void setDeductionsType(int deductionsType) {
		this.deductionsType = deductionsType;
	}
	/**
	 * @return the predictionYears
	 */
	public int getPredictionYears() {
		return predictionYears;
	}
	/**
	 * @param predictionYears the predictionYears to set
	 */
	public void setPredictionYears(int predictionYears) {
		this.predictionYears = predictionYears;
	}
	public double getIncrementAmount() {
		return incrementAmount;
	}
	public void setIncrementAmount(double incrementAmount) {
		this.incrementAmount = incrementAmount;
	}
	public int getNoOfIncrementsInYear() {
		return noOfIncrementsInYear;
	}
	public void setNoOfIncrementsInYear(int noOfIncrementsInYear) {
		this.noOfIncrementsInYear = noOfIncrementsInYear;
	}
	public int getNoOfDeductionsInYear() {
		return noOfDeductionsInYear;
	}
	public void setNoOfDeductionsInYear(int noOfDeductionsInYear) {
		this.noOfDeductionsInYear = noOfDeductionsInYear;
	}
	public int getCurrentPredictionyear() {
		return currentPredictionyear;
	}
	public void setCurrentPredictionyear(int currentPredictionyear) {
		this.currentPredictionyear = currentPredictionyear;
	}
	

}
