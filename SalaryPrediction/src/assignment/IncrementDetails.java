package assignment;

public class IncrementDetails {
	private double sartingSalary;
	private int incrementPercent;
	private int noOfIncrementsInYear;
	private double incrementAmount;
	private int currentPredictionyear;
	
	public double getSartingSalary() {
		return sartingSalary;
	}
	public void setSartingSalary(double sartingSalary) {
		this.sartingSalary = sartingSalary;
	}
	public int getIncrementPercent() {
		return incrementPercent;
	}
	public void setIncrementPercent(int incrementPercent) {
		this.incrementPercent = incrementPercent;
	}
	public int getNoOfIncrementsInYear() {
		return noOfIncrementsInYear;
	}
	public void setNoOfIncrementsInYear(int noOfIncrementsInYear) {
		this.noOfIncrementsInYear = noOfIncrementsInYear;
	}
	public double getIncrementAmount() {
		return incrementAmount;
	}
	public void setIncrementAmount(double incrementAmount) {
		this.incrementAmount = incrementAmount;
	}
	public int getCurrentPredictionyear() {
		return currentPredictionyear;
	}
	public void setCurrentPredictionyear(int currentPredictionyear) {
		this.currentPredictionyear = currentPredictionyear;
	}
}
