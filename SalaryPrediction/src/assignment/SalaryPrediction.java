package assignment;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SalaryPrediction {
	
	
	private SalaryDetails getSalaryDetails() {
		
		Scanner in = new Scanner(System.in);
		
		SalaryDetails  salaryDetailsObj= new SalaryDetails();
		System.out.println("Please enter starting salay : ");
		float sartingSalary = in.nextFloat(); 
		salaryDetailsObj.setSartingSalary(sartingSalary);
        System.out.println("You entered float "+sartingSalary); 
        if(sartingSalary<1) {
        	System.out.println("Please enter starting salay above 1 ruppees");
        	System.exit(0);
        }
        System.out.println("Please enter increment to be received in percent : ");
        int incrementPercent = in.nextInt(); 
        salaryDetailsObj.setIncrementPercent(incrementPercent);
        System.out.println("How frequently is increment received( 4 -quarterly, 2 - half-yearly, 1 - annually etc.)");
        int  noOfIncrementsInYear = in.nextInt();
        if(noOfIncrementsInYear<1) {
        	System.out.println("Please enter frequently is increment received between 1 and 4");
        	System.exit(0);
        }
        salaryDetailsObj.setNoOfIncrementsInYear(noOfIncrementsInYear);
        System.out.println("Please enter deductions on income : ");
        float deductions = in.nextFloat();
        if(deductions<0) {
        	System.out.println("Please enter valid deductions on income");
        	System.exit(0);
        }
        salaryDetailsObj.setDeductions(deductions);
        System.out.println("How frequently is deductions done(4 - quarterly, 2 - half-yearly, 1 - annually etc. )");
        int noOfDeductionsInYear = in.nextInt();
        if(noOfDeductionsInYear<1) {
        	System.out.println("Please enter frequently is deductions received between 1 and 4");
        	System.exit(0);
        }
        salaryDetailsObj.setNoOfDeductionsInYear(noOfDeductionsInYear);
        System.out.println("Please enter prediction for (years) like 5,10,20,30,50 ");
        int predictionYears = in.nextInt();
        salaryDetailsObj.setPredictionYears(predictionYears);
		
        
        return salaryDetailsObj;
	}
	
	private List<IncrementDetails> generateIncrementDetails(SalaryDetails  salaryDetailsObj) {
		
			
			Increment incrementObj = (salaryObj ) -> {
				double sartingSalary = 0;
				double incrementAmt = 0;
				List<IncrementDetails> incrementList = new ArrayList<IncrementDetails>();
				for(int i = 1;i <= salaryObj.getPredictionYears(); i++) {
					IncrementDetails yearlyIncrementDetailsObj = new IncrementDetails();
					if(i == 1) {
						sartingSalary = salaryObj.getSartingSalary();
					}else {
//						incrementAmt = salaryObj.getIncrementAmount();
						sartingSalary =sartingSalary + Math.ceil(incrementAmt);
					}
					
					yearlyIncrementDetailsObj.setSartingSalary(sartingSalary);
					yearlyIncrementDetailsObj.setIncrementPercent(salaryObj.getIncrementPercent());
					yearlyIncrementDetailsObj.setNoOfIncrementsInYear(salaryObj.getNoOfIncrementsInYear());
					incrementAmt = sartingSalary * salaryObj.getNoOfIncrementsInYear() * salaryObj.getIncrementPercent()/100 ;
					yearlyIncrementDetailsObj.setIncrementAmount(Math.ceil(incrementAmt));
					yearlyIncrementDetailsObj.setCurrentPredictionyear(i);
					incrementList.add(yearlyIncrementDetailsObj);
				}
				
				return incrementList;
			};
		
		
		return incrementObj.incrementCalc(salaryDetailsObj);
	}
	
	private List<DeductionDetails> generateDeductionDetials(List<IncrementDetails> incrementList,SalaryDetails  salaryDetailsObj){
		List<DeductionDetails> deductionList = new ArrayList<DeductionDetails>();
		
		incrementList.forEach(incrementObj->{
			int deductionPercent = 0;
			double deductionsAmount = 0;
			DeductionDetails deductionObj = new DeductionDetails();
			deductionObj.setCurrentPredictionyear(incrementObj.getCurrentPredictionyear());
			deductionPercent = (int) ((salaryDetailsObj.getDeductions() /  salaryDetailsObj.getSartingSalary()) * 100);
			if(incrementObj.getCurrentPredictionyear() == 1) {				
				deductionObj.setDeductionsAmount(salaryDetailsObj.getDeductions());
				deductionObj.setDeductionPercent(deductionPercent);
				deductionObj.setSartingSalary(incrementObj.getSartingSalary());
				deductionObj.setNoOfdeductionsInYear(salaryDetailsObj.getNoOfDeductionsInYear());
			}else {
				deductionObj.setNoOfdeductionsInYear(salaryDetailsObj.getNoOfDeductionsInYear());
				deductionObj.setDeductionPercent(deductionPercent);
				deductionObj.setSartingSalary(incrementObj.getSartingSalary());
				deductionsAmount = incrementObj.getSartingSalary() * deductionPercent / 100 ;
				deductionObj.setDeductionsAmount(Math.ceil(deductionsAmount));
			}
			deductionList.add(deductionObj);
			
		});
		return deductionList;
	}
	
	private List<PredictionReportDetails> generatePredictionReport(List<IncrementDetails> incrementList, List<DeductionDetails> deductionList){
		List<PredictionReportDetails> predictionReportList = new ArrayList<PredictionReportDetails>();
		incrementList.forEach(incrementObj->{
			double deductionsAmount = 0;
			double growthAmount = 0;
			double startingsalary = 0;
			PredictionReportDetails predictionObj = new PredictionReportDetails();
			predictionObj.setCurrentPredictionyear(incrementObj.getCurrentPredictionyear());
			predictionObj.setIncrementAmount(incrementObj.getIncrementAmount());
			deductionsAmount = deductionList.get(incrementObj.getCurrentPredictionyear()-1).getDeductionsAmount();
			predictionObj.setDeductionsAmount(deductionsAmount);
			if(incrementObj.getCurrentPredictionyear() == 1) {	
				predictionObj.setSartingSalary(incrementObj.getSartingSalary());
				growthAmount = incrementObj.getSartingSalary()  + incrementObj.getIncrementAmount() - deductionsAmount;
				predictionObj.setSalaryGrowth(growthAmount);
			}else {
				startingsalary = predictionReportList.get(incrementObj.getCurrentPredictionyear()-2).getSalaryGrowth();
				predictionObj.setSartingSalary(startingsalary);
				growthAmount = startingsalary  + incrementObj.getIncrementAmount() - deductionsAmount;
				predictionObj.setSalaryGrowth(growthAmount);
			}
			predictionReportList.add(predictionObj);
		});
		
		return predictionReportList;
	}
	
	public static void main(String[] args) {
		SalaryPrediction predictionObj = new SalaryPrediction();
		SalaryDetails  salaryDetailsObj = predictionObj.getSalaryDetails();
		
		List<IncrementDetails> incrementList= predictionObj.generateIncrementDetails(salaryDetailsObj);
		
//		 System.out.println("incrementListincrementList   " +incrementList);
		System.out.println("=============================================================");
		System.out.println("=================Increment Report============================");
		System.out.println("=============================================================");
		 incrementList.forEach(  (incrementDetailsObj) -> 
		 System.out.print(incrementDetailsObj.getCurrentPredictionyear() +"\t" + 
				 		  incrementDetailsObj.getSartingSalary() + "\t" +
				 		 incrementDetailsObj.getNoOfIncrementsInYear() + "\t" +
				 		incrementDetailsObj.getIncrementPercent() +"\t"  +
				 		incrementDetailsObj.getIncrementAmount() +"\t\n")
				 
				 );
		 System.out.println("=============================================================");
		 System.out.println("=================Deduction Report============================");
		 System.out.println("=============================================================");
		 List<DeductionDetails> deductionList  = predictionObj.generateDeductionDetials(incrementList, salaryDetailsObj);
		 
		 deductionList.forEach(  (deductionDetailsObj) -> 
		 System.out.print(deductionDetailsObj.getCurrentPredictionyear() +"\t" + 
				 		  deductionDetailsObj.getSartingSalary() + "\t" +
				 		 deductionDetailsObj.getNoOfdeductionsInYear() + "\t" +
				 		deductionDetailsObj.getDeductionPercent() +"\t"  +
				 		deductionDetailsObj.getDeductionsAmount() +"\t\n")
				 );
		 
		 System.out.println("=============================================================");
		 System.out.println("=================Prediction Report============================");
		 System.out.println("=============================================================");
		 List<PredictionReportDetails> predictionReportList = predictionObj.generatePredictionReport(incrementList, deductionList);
		 predictionReportList.forEach(  (predictionReportObj) -> 
		 System.out.print(predictionReportObj.getCurrentPredictionyear() +"\t" + 
				 		  predictionReportObj.getSartingSalary() + "\t" +
				 		 predictionReportObj.getIncrementAmount() + "\t" +
				 		predictionReportObj.getDeductionsAmount() +"\t"  +
				 		predictionReportObj.getSalaryGrowth() +"\t\n")
				 );
	
	}

}

@FunctionalInterface  
interface Increment{  
	List<IncrementDetails> incrementCalc(SalaryDetails  salaryObj );  
}  